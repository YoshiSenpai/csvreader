﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;


namespace CSVReader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Logic logic;
        SharedData data;
        ColumnPicker column;

        public SharedData Data { get => data; }

        public MainWindow()
        {
            InitializeComponent();

            data = new SharedData();
            logic = new Logic(data);
            column = new ColumnPicker(data);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            // Instead of closing the window, exit the app
            e.Cancel = true;
            Environment.Exit(Environment.ExitCode);
        }

        private void BrowseButton1_Click(object sender, RoutedEventArgs e)
        {
            data.FilePath1 = Browse();
            data.FileName1 = data.FilePath1.Remove(0, data.FilePath1.LastIndexOf("\\") + 1);
        }

        private void BrowseButton2_Click(object sender, RoutedEventArgs e)
        {
            data.FilePath2 = Browse();
            data.FileName2 = data.FilePath2.Remove(0, data.FilePath2.LastIndexOf("\\") + 1);
        }

        /// <summary>
        /// Opens a file browser.
        /// </summary>
        /// <returns>Returns the path of the selected file, or if none selected returns ""</returns>
        private string Browse()
        {
            Microsoft.Win32.OpenFileDialog fileExplorer = new Microsoft.Win32.OpenFileDialog()
            {
                InitialDirectory = Environment.SpecialFolder.Recent.ToString(),
                Filter = "CSV files (*.csv)|*csv"
            };

            if (fileExplorer.ShowDialog() == true)
            {
                return fileExplorer.FileName;            }
            else
            {
                return "";
            }
        }

        private void StartButton1_Click(object sender, RoutedEventArgs e)
        {
            if (!data.EqChecked && !data.DiffChecked)
            {
                MessageBox.Show("Please select Equals or Differences (or both!)", "Error", MessageBoxButton.OK);
            }
            else
            {
                logic.ReadHeaders(data.FilePath1, data.FileName1, data.Headers);
                logic.ReadHeaders(data.FilePath2, data.FileName2, data.Headers);
                column.RefreshListView();
                column.Show();
            }
        }

        private void CheckBoxEq_Checked(object sender, RoutedEventArgs e)
        {
            data.EqChecked = true;
        }

        private void CheckBoxDiff_Checked(object sender, RoutedEventArgs e)
        {
            data.DiffChecked = true;
        }

        private void CheckBoxEq_Unchecked(object sender, RoutedEventArgs e)
        {
            data.EqChecked = false;
        }

        private void CheckBoxDiff_Unchecked(object sender, RoutedEventArgs e)
        {
            data.DiffChecked = false;
        }

        public void RunLogic()
        {
            logic.RunLogic();
        }
    }
}
