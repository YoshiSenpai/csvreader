﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;

namespace CSVReader
{
    /// <summary>
    /// Interaction logic for ColumnPicker.xaml
    /// </summary>
    public partial class ColumnPicker : Window
    {
        private SharedData data;
        private List<Header> selected1, selected2;
        private CollectionView collection;
        private MainWindow mainW;

        public ColumnPicker(SharedData data)
        {
            InitializeComponent();

            this.data = data;
            selected1 = new List<Header>();
            selected2 = new List<Header>();
            mainW = (MainWindow)Application.Current.MainWindow;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //  Instead of closing, hide the window
            e.Cancel = true;
            this.Hide();

            //  Clear all Lists
            selected1.Clear();
            selected2.Clear();
            data.Headers.Clear();
            ColumnList.ItemsSource = null;
            collection.GroupDescriptions.Clear();
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            data.Indexes1.Clear();
            data.Indexes2.Clear();

            foreach (Header h in data.Headers)
            {
                if (!h.Selected && h.File == data.FileName1)
                {
                    data.Indexes1.Add(h.Index);
                }
                else if (!h.Selected && h.File == data.FileName2)
                {
                    data.Indexes2.Add(h.Index);
                }
                else if (h.Selected && h.File == data.FileName1)
                {
                    selected1.Add(h);
                }
                else if (h.Selected && h.File == data.FileName2)
                {
                    selected2.Add(h);
                }
            }

            try
            {
                //  Checks if the amount of selected columns in each file doesn't match
                if (selected1.Count != selected2.Count)
                    throw new ColumnCountNotMatchingException("Amount of selected columns do not match");

                this.Close();

                mainW.RunLogic();
            }

            //  If the Exception got thrown...
            catch (ColumnCountNotMatchingException ex)
            {
                //  Show a MessageBox to the user with information about the exception
                //  Then Clear all selections
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK);
                selected1.Clear();
                selected2.Clear();
            }


        }

        private void PrintAllColumns_Checked(object sender, RoutedEventArgs e)
        {
            data.PrintAllColumns = true;
        }

        private void PrintAllColumns_UnChecked(object sender, RoutedEventArgs e)
        {
            data.PrintAllColumns = false;
        }

        /// <summary>
        /// Assigns the ItemsSource and groups items together based on source file name.
        /// </summary>
        public void RefreshListView()
        {
            ColumnList.ItemsSource = data.Headers;

            //  Set up ListView grouping by the "File" property of the Header class
            //  This means that each header belonging to file1 is grouped together under that file's name
            collection = (CollectionView)CollectionViewSource.GetDefaultView(ColumnList.ItemsSource);
            PropertyGroupDescription description = new PropertyGroupDescription("File");
            collection.GroupDescriptions.Add(description);
        }

        //  Custom Exception for ColumnPicker, used when user doesn't select the same amount of columns from both files
        private class ColumnCountNotMatchingException : Exception
        {
            public ColumnCountNotMatchingException()
            {
            }

            public ColumnCountNotMatchingException(string message)
                : base(message)
            {
            }
        }
    }
}
