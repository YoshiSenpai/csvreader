﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Windows;

namespace CSVReader
{
    /// <summary>
    /// The Logic class handles all reading, wrtiting, and comparing of .csv files
    /// </summary>
    public class Logic
    {
        private SharedData data;

        public Logic(SharedData data)
        {
            this.data = data;
        }

        /// <summary>
        /// Reads, Compares, and Writes two files.
        /// </summary>
        public void RunLogic()
        {
            //  Read both files in parallel, save the data read to lists
            Parallel.Invoke(
                () => ReadFile(data.FilePath1, data.List1, data.CompareList1, data.FileName1),
                () => ReadFile(data.FilePath2, data.List2, data.CompareList2, data.FileName2));

            //  Compare the data from both files, and save the results to different lists
            CompareFile(data.List1, data.List2, data.ResultsEq, data.ResultsDiff, data.CompareList1, data.CompareList2);

            //  If Equals checkmark is checked, write the results to desktop
            if (data.EqChecked)
            {
                WriteFile(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), data.ResultsEq, "Equals", true);
            }

            //  If Differences checkmark is checked, write the results to desktop 
            if (data.DiffChecked)
            {
                WriteFile(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), data.ResultsDiff, "Differences", false);
            }

            //  Clear the lists which were used for storing read data
            Reset();
        }

        /// <summary>
        /// Clears all lists.
        /// </summary>
        public void Reset()
        {
            data.List1.Clear();
            data.List2.Clear();
            data.CompareList1.Clear();
            data.CompareList2.Clear();
            data.ResultsDiff.Clear();
            data.ResultsEq.Clear();
        }

        /// <summary>
        /// Compares two files(which have been read to lists), and finds equalities and/or differences between them.
        /// Uses two lists for each file depending on whether or not PrintAllColumns is true.
        /// </summary>
        /// <param name="list1">First list to compare, or the original list if PrintAllColumns is true</param>
        /// <param name="list2">Second list to compare, or the original list if PrintAllColumns is true</param>
        /// <param name="resultsEq">List where equalities will be saved</param>
        /// <param name="resultsDiff">List where differences will be saved</param>
        /// <param name="compare1">First list to compare, if PrintAllColumns is true</param>
        /// <param name="compare2">Second list to compare, if PrintAllColumns is true</param>
        public void CompareFile(List<Line> list1, List<Line> list2, List<Line> resultsEq, List<Line> resultsDiff, List<Line> compare1, List<Line> compare2)
        {
            if ((data.EqChecked || data.DiffChecked) && !data.PrintAllColumns)
            {
                //  Remove header lines
                data.ResultsEq.Add(data.List1.ElementAt(0));
                resultsDiff.Add(list1.ElementAt(0));
                resultsDiff.Add(list2.ElementAt(0));
                list2.RemoveAt(0);
                list1.RemoveAt(0);

                // Merge the two lists
                list1.AddRange(list2);

                // Sort the merged list
                list1.Sort((l1, l2) => l1.FieldsToString.CompareTo(l2.FieldsToString));

                for (int i = 1; i < list1.Count; i++)
                {
                    // Check if the lines are equal, and that they are not from the same source file
                    if (list1.ElementAt(i).FieldsToString.Equals(list1.ElementAt(i - 1).FieldsToString)
                        && !list1.ElementAt(i).FileName.Equals(list1.ElementAt(i - 1).FileName))
                    {
                        if (data.EqChecked)
                            resultsEq.Add(list1.ElementAt(i));
                        i++;
                    }
                    else if (data.DiffChecked)
                        resultsDiff.Add(list1.ElementAt(i));
                }

                data.ResultsDiff = resultsDiff.OrderBy(x => x.FileName).ThenBy(x => x.LineNumber).ToList();
            }

            if ((data.EqChecked || data.DiffChecked) && data.PrintAllColumns)
            {
                //  Remove header lines and add them to result files, then make a copy of list1
                resultsEq.Add(list1.ElementAt(0));
                resultsDiff.Add(list1.ElementAt(0));
                resultsDiff.Add(list2.ElementAt(0));
                list2.RemoveAt(0);
                list1.RemoveAt(0); 
                compare1.RemoveAt(0);
                compare2.RemoveAt(0);
                List<Line> list1Copy = new List<Line>(list1.ToList());

                // Merge the different lists
                Parallel.Invoke(() => list1.AddRange(list2), () => compare1.AddRange(compare2));

                // Sort the list used for comparing
                compare1.Sort((l1, l2) => l1.FieldsToString.CompareTo(l2.FieldsToString));

                for (int i = 1; i < compare1.Count; i++)
                {
                    // Check if the lines are equal, and that they are not from the same source file
                    if (compare1.ElementAt(i).FieldsToString.Equals(compare1.ElementAt(i - 1).FieldsToString)
                        && !compare1.ElementAt(i).FileName.Equals(compare1.ElementAt(i - 1).FileName))
                    {
                        if (data.EqChecked)
                        {
                            Line result = null;

                            // Find the matching original line for the line we compared
                            if (compare1.ElementAt(i).FileName.Equals(data.FileName1))
                            {
                                result = list1Copy.Find(
                                    x => x.LineNumber.Equals(compare1.ElementAt(i).LineNumber)
                                    && x.FileName.Equals(compare1.ElementAt(i).FileName));
                            }
                            else
                            {
                                result = list1Copy.Find(
                                    x => x.LineNumber.Equals(compare1.ElementAt(i - 1).LineNumber)
                                    && x.FileName.Equals(compare1.ElementAt(i - 1).FileName));
                            }

                            if (result != null)
                                resultsEq.Add(result);
                        }

                        i++;
                    }
                    else if (data.DiffChecked)
                    {
                        // Find the matching original line for the line we compared
                        var result = list1.Find(
                            x => x.LineNumber.Equals(compare1.ElementAt(i).LineNumber) 
                            && x.FileName.Equals(compare1.ElementAt(i).FileName));

                        if (result != null)
                            resultsDiff.Add(result);
                    }
                }

                data.ResultsDiff = resultsDiff.OrderBy(x => x.FileName).ThenBy(x => x.LineNumber).ToList();
            }
        }

        
        /// <summary>
        /// Reads the first line from a .csv file.
        /// Creates a Header<see cref="Header"/> object for each column. 
        /// </summary>
        /// <param name="path">Path of the file we are reading from</param>
        /// <param name="file">Name of the file we are reading from</param>
        /// <param name="headers">List we are storing the Headers in</param>
        public void ReadHeaders(string path, string file, List<Header> headers)
        {
            try
            {
                StreamReader reader = new StreamReader(path);

                //  Read the first line (header line) and split it into an array of strings
                string[] line = reader.ReadLine().ToLower().Split(',');

                foreach (string s in line)
                {
                    //  Add a new Header to the Headers list
                    //  Variable s will be the name of the Header object and variable file will be the name of the file this Header belongs to
                    headers.Add(new Header(s, file, Array.IndexOf(line, s)));
                }

                reader.Close();
            }

            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "ERROR", MessageBoxButton.OK);

               // return null;
            }

        }


        /// <summary>
        /// Reads a file to a list.
        /// Deletes unwanted columns from the lines read, unless PrintAllColumns is true.
        /// If PrintAllColumns is true, saves original versions of the lines as well.
        /// </summary>
        /// <param name="path">Path of the file we are reading from</param>
        /// <param name="list">List we are reading to</param>
        /// <param name="compare">List we are reading to if PrintAllColumns is true</param>
        /// <param name="fileName">Name of the file we are reading from</param>
        public void ReadFile(string path, List<Line> list, List<Line> compare, string fileName)
        {

            try
            {
                StreamReader reader = new StreamReader(path);
                int i = 0;

                //  Loop until there is no more lines to read
                while (!reader.EndOfStream)
                {
                    //  Read one line at a time, splitting it into an array which is then converted to a list for better functionality
                    Line line = new Line(reader.ReadLine().ToLower().Split(',').ToList(), fileName, i);
                    //  Make an identical copy of the line read
                    Line lineCopy = new Line(line.Fields.ToList(), fileName, i);

                    foreach (string field in lineCopy.Fields)
                    {
                        // Check which file we are reading
                        if (fileName == data.FileName1)
                        {
                            if (data.Indexes1.Contains(lineCopy.Fields.IndexOf(field)))
                                line.Fields.Remove(field);
                        }
                        else if (fileName == data.FileName2)
                        {
                            if (data.Indexes2.Contains(lineCopy.Fields.IndexOf(field)))
                                line.Fields.Remove(field);
                        }
                    }

                    // Save string versions of the modified arrays inside the Line class
                    line.FieldsToString = line.ArrayToString(line.Fields.ToArray());
                    lineCopy.FieldsToString = lineCopy.ArrayToString(lineCopy.Fields.ToArray());

                    if (!data.PrintAllColumns)
                    {
                        list.Add(line);
                    }

                    else if (data.PrintAllColumns)
                    {
                        compare.Add(line);
                        list.Add(lineCopy);
                    }

                    i++;
                }
                reader.Close();
                i = 0;
            }

            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "ERROR", MessageBoxButton.OK);
            }
        }

        /// <summary>
        /// Writes all items in a list to a .csv file. Uses "," as a separator.
        /// </summary>
        /// <param name="path">Path of the output</param>
        /// <param name="list">List to write from</param>
        /// <param name="name">Name of the file we are writing</param>
        /// <param name="isEqList">Is this list the EQ list?</param>
        public void WriteFile(string path, List<Line> list, string name, bool isEqList)
        {
            path = path + "\\" + name + ".csv";

            StreamWriter writer = new StreamWriter(path);

            //  Loop through each line in the list provided
            foreach (var line in list)
            {
                try
                {
                    if (!isEqList)
                    {
                        if (line.LineNumber != 0)
                            writer.WriteLine(line.FieldsToString + "," + line.FileName);
                        else
                            writer.WriteLine(line.FieldsToString + ",source file"); 
                    }
                    else
                        writer.WriteLine(line.FieldsToString);

                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString(), "ERROR", MessageBoxButton.OK);
                }
            }

            writer.Close();
        }
    }
}
