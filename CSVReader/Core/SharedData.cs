﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace CSVReader
{

    /// <summary>
    /// Stores shared data between objects.
    /// </summary>
    public class SharedData : INotifyPropertyChanged
    {
        private string filePath1 = string.Empty;
        private string filePath2 = string.Empty;

        public string FilePath1
        {
            get => filePath1;
            set
            {
                if (value != filePath1)
                {
                    filePath1 = value;
                    OnPropertyChanged("FilePath1");
                }
            }
        }

        public string FilePath2
        {
            get => filePath2;
            set
            {
                if (value != filePath2)
                {
                    filePath2 = value;
                    OnPropertyChanged("FilePath2");
                }
            }
        }

        public string FileName1 { get; set; }
        public string FileName2 { get; set; }

        public List<Line> List1 { get; set; }
        public List<Line> List2 { get; set; }
        public List<Line> CompareList1 { get; set; }
        public List<Line> CompareList2 { get; set; }
        public List<Line> ResultsEq { get; set; }
        public List<Line> ResultsDiff { get; set; }

        public List<Header> Headers { get; set; }

        public List<int> Indexes1 { get; set; }
        public List<int> Indexes2 { get; set; }

        public bool EqChecked { get; set; }
        public bool DiffChecked { get; set; }
        public bool PrintAllColumns { get; set; }



        public SharedData()
        {
            List1 = new List<Line>();
            List2 = new List<Line>();
            CompareList1 = new List<Line>();
            CompareList2 = new List<Line>();
            ResultsDiff = new List<Line>();
            ResultsEq = new List<Line>();

            Headers = new List<Header>();

            Indexes1 = new List<int>();
            Indexes2 = new List<int>();
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
