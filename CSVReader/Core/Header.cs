﻿namespace CSVReader
{
    /// <summary>
    /// A pure data class, provides no functionality, used for storing information of a header
    /// </summary>
    public class Header
    {
        public string Name { get; private set; }
        public bool Selected { get; set; }
        public string File { get; private set; }
        public int Index { get; private set; }

        //  Header constructor, requires two arguments, the name of the header and the name of the file the header is from
        public Header(string name, string file, int index = 0)
        {
            Name = name;
            Selected = false;
            File = file;
            Index = index;
        }
    }
}
