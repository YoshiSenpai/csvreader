﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSVReader
{
    /// <summary>
    /// Stores information of a line
    /// </summary>
    public class Line
    {
        public int LineNumber { get; set; }
        public List<string> Fields { get; set; }
        public string FieldsToString { get; set; }
        public string FileName { get; set; }

        public Line()
        {

        }

        public Line(List<string> list, string fileName, int line)
        {
            Fields = list;
            FileName = fileName;
            LineNumber = line;
        }

        /// <summary>
        /// Creates a string from an array
        /// </summary>
        /// <param name="arr">The array we want to create a string out of</param>
        /// <returns>Returns a string version of the array given</returns>
        public string ArrayToString(string[] arr)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string s in arr)
            {
                if (s != arr[arr.Length-1])
                {
                    sb.Append(s + ",");
                }
                else
                {
                    sb.Append(s);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Splits a .csv line to an array
        /// </summary>
        /// <param name="s">The String we want to split to an array</param>
        /// <returns>Returns an array from the string given</returns>
        public string[] StringToArray(string s)
        {
            return s.Split(',');
        }
    }
}
