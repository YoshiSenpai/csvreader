﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Text;
using CSVReader;
using System.Threading.Tasks;
using System.Linq;

namespace CSVReaderTests
{
    [TestClass]
    public class LogicTest
    {
        private Logic logic = new Logic(new SharedData() {

        });
        private string desktopDir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

        //[TestMethod]
        public void ReadTest()
        {

        }

        [TestMethod]
        public void CompareTest()
        {

        }

        [TestMethod]
        public void SpeedTest()
        {

        }

        private List<Line> RandomList(string[] strings, string name, int columns = 3, int lines = 300, int seed = 5109385)
        {
            List<Line> list = new List<Line>();
            Random r = new Random(seed);

            for (int i1 = 0; i1 < lines; i1++)
            {
                Line l = new Line(new List<string>(), name, i1);

                for (int i2 = 0; i2 < columns; i2++)
                {
                    l.Fields.Add(strings[r.Next(0, strings.Length)]);
                }
                l.FieldsToString = l.ArrayToString(l.Fields.ToArray());
                list.Add(l);
            }

            return list;
        }

        private string[] RandomStringGen(int arraySize = 1000, int stringSize = 5, int seed = 9865)
        {
            string[] strings = new string[arraySize];
            char[] chars = { 'a', 'b', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            StringBuilder sb = new StringBuilder();
            Random r = new Random(seed);

            for (int i = 0; i < arraySize; i++)
            {
                sb.Clear();

                for (int x = 0; x < stringSize; x++)
                {
                    sb.Append(chars[r.Next(0, chars.Length)]);
                }

                strings[i] = sb.ToString();
            }

            return strings;
        }
    }
}
